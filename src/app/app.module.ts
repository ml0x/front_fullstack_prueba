import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarComponent } from './Encuesta/listar/listar.component';
import { AgregarComponent } from './Encuesta/agregar/agregar.component';
import { FormsModule } from '@angular/forms';
import { ServiceService } from '../app/Service/service.service';
import { HttpClientModule } from '@angular/common/http';
import { AcercaDeComponent } from './Encuesta/acerca-de/acerca-de.component';

@NgModule({
  declarations: [AppComponent, ListarComponent, AgregarComponent, AcercaDeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent],
})
export class AppModule {}
