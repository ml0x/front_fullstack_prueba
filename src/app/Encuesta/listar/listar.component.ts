import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Encuesta } from 'src/app/Modelo/Encuesta';
import { ServiceService } from '../../Service/service.service';
import {AppComponent} from "../../app.component"

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  encuesta: Encuesta[];
  constructor(private service: ServiceService, private router: Router) {}
  ngOnInit(): void {
    console.log("listar")

      this.service.getResultados().subscribe((response:any) => {
      this.encuesta = response.data;
    });
  }
}
