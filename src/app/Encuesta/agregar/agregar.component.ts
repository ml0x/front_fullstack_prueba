import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';
import {Encuesta} from 'src/app/Modelo/Encuesta';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css'],
})
export class AgregarComponent implements OnInit {

  encuesta:Encuesta;
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    console.log("agregar")

    this.encuesta = new Encuesta();
  }

  Guardar(encuesta:Encuesta){
    console.log(encuesta)
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (encuesta.correo!= undefined && encuesta.correo!= "" && emailRegex.test(encuesta.correo.toString())) {
      this.service.getResultados().subscribe((response:any) => {
        if(response.data.find((element:any)=> element.correo.toLowerCase()=== encuesta.correo.toLowerCase())){
          alert("El correo Ingresado ya se encuentra registrado");
        }else if (encuesta.estiloMusical == "" || encuesta.estiloMusical==undefined){
          alert("Debes Agregar Campo Estilo Musical");
        }else{
           this.service.createRegistro(encuesta).subscribe((response:any) => {
              alert(response.message);
              this.router.navigate(["listar"]);
            })
         }
      });
    } else {
      alert("Debes Ingresar Un Correo Valido");
    }
  }
}
