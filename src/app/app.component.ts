import { Component, ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class AppComponent {
  title = 'prueba';
  hideMenu = false;

  constructor(private router:Router){}

    ngOnInit(): void {

    }

    Listar(){
      this.router.navigate(["listar"])
    }

    Agregar(){
      this.router.navigate(["agregar"])
    }

    AcercaDe(){
      this.router.navigate(["acercaDe"])
    }


}
