import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './Encuesta/agregar/agregar.component';
import { ListarComponent } from './Encuesta/listar/listar.component';
import { AcercaDeComponent } from './Encuesta/acerca-de/acerca-de.component';

const routes: Routes = [
  {path:'listar', component:ListarComponent},
  {path:'agregar', component:AgregarComponent},
  {path:'acercaDe', component:AcercaDeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {


 }
