import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Encuesta } from '../Modelo/Encuesta';

@Injectable({
  providedIn: 'root',
})

export class ServiceService {

  constructor(private http: HttpClient) {}
  Url="http://localhost:8080/Prueba/getEncuesta";
  Url2="http://localhost:8080/Prueba/addRegistro";

  getResultados(){
    return this.http.get<Encuesta[]>(this.Url);
  }

  createRegistro(encuesta:Encuesta){
    return this.http.post<Encuesta>(this.Url2, encuesta) ;
  }

}
